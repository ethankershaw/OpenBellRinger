﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FluentScheduler;

namespace OpenBellRinger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MediaPlayer mediaPlayer = new MediaPlayer();
        
        public MainWindow()
        {
            InitializeComponent();
            var registry = new Registry();
            JobManager.Initialize(registry);
        }

        private void playSound() {
            this.Dispatcher.Invoke(() =>
            {
                System.Media.SystemSounds.Exclamation.Play();
                updateNextRing();
            });
        }

        private void updateNextRing()
        {
            lblNextRing.Content = $"Next Ring: {Properties.Settings.Default.Schedule?.Min(s=>s.NextRing).ToString() ?? "[none]"}";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            playSound();
        }

        static MainWindow()
        {
            if (Properties.Settings.Default.Schedule == null)
            {
                Properties.Settings.Default.Schedule = new RingSchedule
                {
                    new ScheduledRing() { DayOfWeek = DayOfWeek.Sunday, Hour = 9, Minute = 40 },
                    new ScheduledRing() { DayOfWeek = DayOfWeek.Sunday, Hour = 9, Minute = 45 },
                    new ScheduledRing() { DayOfWeek = DayOfWeek.Wednesday, Hour = 19, Minute = 40 },
                    new ScheduledRing() { DayOfWeek = DayOfWeek.Wednesday, Hour = 19, Minute = 45 }
                };
                Properties.Settings.Default.Save();
            }
        }
    }
}
