﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenBellRinger
{
    [Serializable]
    public class ScheduledRing
    {
        public Guid ID { get; set; }
        public string SoundFile { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }

        public ScheduledRing()
        {
            ID = Guid.NewGuid();
        }
        
        public DateTime NextRing //adapted from https://stackoverflow.com/questions/6346119/datetime-get-next-tuesday
        {
            get
            {
                DateTime start = DateTime.Now.Hour < Hour ? DateTime.Today : DateTime.Today.AddDays(1);

                // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
                int daysToAdd = ((int)DayOfWeek - (int)start.DayOfWeek + 7) % 7;
                return start.AddDays(daysToAdd).AddHours(Hour).AddMinutes(Minute);
            }
        }
    }
}
